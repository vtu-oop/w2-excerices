﻿// See https://aka.ms/new-console-template for more information

using Shapes;

List<Shape> shapes = new List<Shape>();

Triangle triangle = new Triangle(3, 4);
shapes.Add(triangle);

Rectangle rectangle = new Rectangle(5, 6);
shapes.Add(rectangle);

Circle circle = new Circle(7);
shapes.Add(circle);

List<double> surfaces = new List<double>();
foreach (var shape in shapes)
{
    surfaces.Add(shape.CalculateSurface());
}

surfaces.ForEach(e => Console.WriteLine(e));