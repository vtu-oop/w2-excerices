namespace Shapes;

public abstract class Shape
{
    protected double Height { get; }
    protected double Width { get;  }

    protected Shape(double height, double width)
    {
        Height = height;
        Width = width;
    }
    public abstract double CalculateSurface();
}