namespace Shapes;

public class Circle : Shape
{
    public Circle(double radius) : base(radius, radius)
    {
        
    }
    
    public override double CalculateSurface()
    {
        return Math.Round(Math.PI * Height * Width, 2);
    }
}