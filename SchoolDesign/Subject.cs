namespace SchoolDesign;

public class Subject
{
    public int LecturesCount { get; set; }
    public int LabsCount { get; set; }
}